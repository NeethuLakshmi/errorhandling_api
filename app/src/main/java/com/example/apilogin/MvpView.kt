package com.example.apilogin


interface MvpView {
    fun showLoading()
    fun hideLoading()
    fun netWorkConnected():Boolean
    fun showMessage(message:String)
}