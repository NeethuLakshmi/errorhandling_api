package com.example.apilogin

interface ILoginPresenter {
    fun callLoginApi(emailId:String,password:String,providerType:String)
}