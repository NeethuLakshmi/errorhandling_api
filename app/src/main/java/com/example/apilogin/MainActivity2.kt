package com.example.apilogin

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_main2.*
class MainActivity2 : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        logoutTextview.setOnClickListener {
            SharedPreferencemanager.getInstance(this).clear()

        }
    }
    override fun onStart() {
        super.onStart()
        if (! SharedPreferencemanager.getInstance(this).isLoggin) {
            val intent = Intent(applicationContext, MainActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
            startActivity(intent)
            // Toast.makeText(applicationContext,"activity 2",Toast.LENGTH_LONG).show()
        }
    }

}