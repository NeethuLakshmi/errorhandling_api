package com.example.apilogin

interface LoginView: MvpView {
    fun onSuccess(loginBase: LoginResponse)

    fun onError(error: Error)

}