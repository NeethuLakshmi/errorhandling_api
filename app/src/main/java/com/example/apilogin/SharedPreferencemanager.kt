package com.example.apilogin

import android.content.Context

class SharedPreferencemanager private constructor(private val mCtx: Context) {
    val isLoggin: Boolean
        get() {
            val sharedPrefrences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE)
            return sharedPrefrences.getInt("id", -1) == -1
        }
    val user: User
        get() {
            val sharedPrefrences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE)
            return User(

                sharedPrefrences.getInt("user_id ", -1),
                sharedPrefrences.getString("email_id", null).toString(),
                sharedPrefrences.getInt("user_type", 1)
            )
        }

    fun saveUser(user: User) {

        val sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE)
        val editor = sharedPreferences.edit()
        editor.putString("email_id", user.email_id)
        editor.putString("user_id", user.user_id.toString())
        editor.putInt("user_type", user.user_type).toString()
        editor.apply()
    }

    fun clear() {
        val sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE)
        val editor = sharedPreferences.edit()
        editor.clear()
        editor.apply()
    }

    companion object {
        val SHARED_PREF_NAME = "sharedpreff_name"
        private var mInstance: SharedPreferencemanager? = null

        @Synchronized
        fun getInstance(mCtx: Context): SharedPreferencemanager {
            if (mInstance == null) {
                mInstance = SharedPreferencemanager(mCtx)
            }
            return mInstance as SharedPreferencemanager
        }

    }
}
