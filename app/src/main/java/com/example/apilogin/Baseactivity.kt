package com.example.apilogin

import android.annotation.SuppressLint
import android.content.Context
import android.net.ConnectivityManager
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*

open class Baseactivity : AppCompatActivity(),MvpView {
    @SuppressLint("ServiceCast")
    fun isNetworkConnected() :Boolean {
        val connectivityManager = this.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val networkInfo = connectivityManager.activeNetworkInfo
        return networkInfo?.isConnectedOrConnecting  == true

    }

    override fun showLoading() {
      progressBarBaseActivity?.visibility = View.VISIBLE
    }

    override fun hideLoading() {
        progressBarBaseActivity?.visibility = View.GONE
    }

    override fun netWorkConnected(): Boolean {
       val connectivityManager = this.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val networkInfo = connectivityManager.activeNetworkInfo
        return networkInfo?.isConnectedOrConnecting == true


    }

    override fun showMessage(message: String) {
       Toast.makeText(applicationContext,message,Toast.LENGTH_LONG).show()
    }
}