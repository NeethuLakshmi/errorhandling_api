package com.example.apilogin

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import com.google.gson.GsonBuilder
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : Baseactivity(),LoginView {
    val builder = GsonBuilder()
    val gson = builder.serializeNulls().create()
    private val loginPresenter=LoginPresenter(this,this)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        progressBar.visibility = View.GONE
        loginbutton.setOnClickListener {
            val email = EmaillogineditText.text.toString().trim()
            val password = logineditTextPassword.text.toString().trim()
            loginPresenter.callLoginApi(email,password,"1")
        }
    }

    override fun onSuccess(loginBase: LoginResponse) {
      showMessage(loginBase.message!!)
    }

    override fun onError(error: Error) {
        showMessage(error.message!!)
    }
    override fun onBackPressed() {
        val intent = Intent(Intent.ACTION_MAIN)
        intent.addCategory(Intent.CATEGORY_HOME)
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
        startActivity(intent)

    }
}