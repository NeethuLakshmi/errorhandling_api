package com.example.apilogin

import android.content.Context
import android.content.Intent
import androidx.core.content.ContextCompat
import androidx.core.content.ContextCompat.startActivity
import com.google.gson.GsonBuilder
import com.google.gson.JsonObject
import retrofit2.Call
import retrofit2.Response
import retrofit2.Callback
class LoginPresenter(var iLoginView: LoginView,var context: Context):ILoginPresenter {
    val builder = GsonBuilder()
    val gson = builder.serializeNulls().create()
    override fun callLoginApi(emailId: String, password: String, providerType: String) {
        iLoginView.showLoading()
        if (iLoginView.netWorkConnected()){
            RetrofitClient.instance.login(emailId,password,1).enqueue(object :Callback<JsonObject>
            {
                override fun onResponse(call: Call<JsonObject>, response: Response<JsonObject>) {
            when{
                response.code()==400 ->{
                    val logiBase = gson.fromJson(response.errorBody()?.charStream(),Error::class.java)
                    iLoginView.onError(logiBase)
                }
                response.code() == 200 ->{
                    val logiBase = gson.fromJson(response.body()?.toString(),LoginResponse::class.java)
                    iLoginView.onSuccess(logiBase)
//                    val intent = Intent(this@LoginPresenter,MainActivity2::class.java)
//                    startActivity(intent)
                  
                }else -> { iLoginView.showMessage(context.resources.getString(R.string.something_went))}
            }
                }
                override fun onFailure(call: Call<JsonObject>, t: Throwable) {
                    TODO("Not yet implemented")
                }
            })
        } else{
            iLoginView.hideLoading()
            iLoginView.showMessage(context.resources.getString(R.string.no_internet))
        }
}}


